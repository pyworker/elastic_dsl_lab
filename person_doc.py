"""
module for discribe data model
"""


from hashlib import md5
from datetime import datetime

from elasticsearch_dsl import DocType, Text, Boolean, Date, Integer
from elasticsearch_dsl.connections import connections

connections.create_connection(hosts=[{'host': '127.0.0.1', 'port':9200},])

class PersonDoc(DocType):
    name = Text()
    sername = Text()
    registrated = Date() 
    bday = Date()
    age = lines = Integer()
    is_leader = Boolean()
    user_id = Text()

    class Meta:
        # index name
        index = 'dsl_person'

        # index type (extra)
        doc_type = 'users'

    def save(self, ** kwargs):
        """
        override save method
        add registrated date and calculate id
        """

        self.registrated = datetime.now()
        hash_str = ''.join([str(self.name.split()), str(self.sername.split())])
        self.user_id = md5(hash_str.encode('utf-8')).hexdigest()
        return super(PersonDoc, self).save(** kwargs)