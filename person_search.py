"""
module for work with persons DB
"""

from person_doc import PersonDoc


class PersonSearch:
    """docstring for ElasticMaster"""

    def __init__(self):
        # create mapping
        PersonDoc.init()

    def get_persons(self):
        """
        get all person, sorted by adding order
        Returns:
            Response Object (list of PersonDoc objects)
            []: if not find Objects in elastic 
        """
        query = PersonDoc.search().sort('-registrated')
        response = query.execute()
        if not self.count_person(persons=response):
            return []
        return response

    def get_person_by_name(self, name):
        """
        get person by name
        Args:
            name: str: person name
        Returns:
            PersonDoc Object: if find person in elastic
            None: if person not finded
        """
        search = PersonDoc.search().filter('term', name=name)
        response = search.execute()
        if not self.count_person(persons=response):
            return None
        return response[0]

    def add_person(self, name, sername, age, bday, is_leader):
        """
        add new person to index
        Returns:
            True: if person add to elastic
        """
        person_doc = PersonDoc()
        person_doc.name = name
        person_doc.sername = sername
        person_doc.age = age
        person_doc.bday = bday
        person_doc.is_leader = is_leader
        return person_doc.save()

    def update_person_age(self, person, age):
        """
        change person age
        Args:
            person: PersonDoc, info about person
            age: int, new age
        Returns:
            True: if person age updated
            False: if person dont changed  
        """
        if not isinstance(person, PersonDoc):
            return False

        person.age = age
        person.save()
        return True

    def update_person_tel(self, person, tel):
        """
        update person telephone list
        Args:
            person: PersonDoc, info about person
            tel: str, new telephone number
        Returns:
            True: document updated
            False: if person dont changed  
        """
        if not isinstance(person, PersonDoc):
            return False

        ## get person elatic ID
        # id = person.meta.id
        # person_doc = PersonDoc(meta={'id': id})

        try:
            # person already have field "tel"
            person.tel.append(tel)
        except AttributeError as attr_err:
            # person dont have field "tel"
            person.tel = [tel]
        except Exception as ex:
            raise Exception('failed to update person tel: %s' % ex)

        # save() return False because new document dint added 
        person.save()
        return True

    def del_person_by_name(self, name):
        """
        delete person by name
        Args:
            name: str, person name
        Returns: 
            True: if document successfully deleted
            False: if document not found
        """
        person = self.get_person_by_name(name=name)
        if not person:
            return False

        person.delete()
        return True

    def del_person(self, person):
        """
        delete person from elasticsearch
        Args:
            person: PersonDoc, info about person
        Returns:
            True: if person deleted
        """
        person.delete()
        return True

    def parse_person(self, person):
        """
        parse hits in response
        Args:
            person: PersonDoc object
        """
        parse_str = '%2s %15s %7s %7s' % (
                    person.meta.id,
                    person.meta.score,
                    person.name,
                    person.sername)
        return parse_str

    def parse_response(self, response):
        print(response.hits.total, 'hits total')
    
        print('%2s %15s %7s %7s' % ('id', 'score', 'name', 'sername'))
        for hit in response:
            print(self.parse_person(person=hit))

    def parse_raw(self, response):
        """
        get raw elastic response without DocType
        """
        return response.to_dict()

    def count_person(self, persons):
        if not persons:
            return None
        return persons.hits.total

