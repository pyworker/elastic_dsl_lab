import unittest
from time import sleep 
from datetime import datetime

from person_search import PersonSearch 


class TestPersonSearch(unittest.TestCase):
    
    def test_currect_work(self):
        """
        (functional testing)
        test module currect work
        """
        em = PersonSearch()

        # prepare index (clean)
        persons = em.get_persons()
        if em.count_person(persons=persons):
            for person in persons:
                em.del_person(person)

        # add new pereson to elastic
        if em.add_person(
                name='mike',
                sername='bobo',
                age=11,
                is_leader=True,
                bday=datetime.now()
            ):
            print('new person added')

        sleep(1)

        mike = em.get_person_by_name(name='mike')
        print('Mike : %s' % em.parse_person(person=mike))

        r1 = em.update_person_age(person=mike, age=100)
        r2 = em.update_person_tel(person=mike, tel='11111111111')
        r3 = em.update_person_tel(person=mike, tel='22222222222')
        sleep(1)
        mike = em.get_person_by_name(name='mike')
        print(em.parse_raw(response=mike))


        print('ready to delete person: %s' % mike.name)
        r4 = em.del_person_by_name(name=mike.name)

        ans = (r1 and r2 and r3 and r4)
        should_ans = True
        self.assertEqual(ans, should_ans)



if __name__ == '__main__':
    unittest.main()